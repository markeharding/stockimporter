<?php
namespace StockImporter;

use PDO;

class Import
{

    /**
     * @var array - the dataset
     */
    protected $dataset = [];

    /**
     * @param PDO $pdo
     * @param Config $config
     * @return void
     */
    public function __construct($pdo = null, $config = null)
    {
        $this->config = $config ?: new Config();
        $this->pdo = $pdo ?: new PDO("mysql:host={$this->config->mysql->host};dbname={$this->config->mysql->db};charset=utf8", $this->config->mysql->user, $this->config->mysql->password);
    }

    /**
     * Sets the dataset. Can be a CSV, JSON etc
     * @param Dataset $dataset
     * @return $this
     */
    public function setDataset($file)
    {
        $dataset = $file->getDataset();
        if (!$dataset) {
            throw new Exceptions\EmptyDatasetException();
        }
        $this->dataset = $dataset;
        return $this;
    }

    /**
     * Import the dataset into the databse
     * @return $this
     */
    public function import()
    {
        foreach ($this->dataset as $row) {
            $statement = $this->pdo->prepare("INSERT INTO stock (code, name, description, stock, cost, currency, discontinued) VALUES (:code, :name, :description, :stock, :cost, 'GBP', :discontinued)");
            $statement->execute([
              ':code' => $row['Product Code'],
              ':name' => $row['Product Name'],
              ':description' => $row['Product Description'],
              ':stock' => $row['Stock'],
              ':cost' =>  $row['Cost in GBP'],
              ':discontinued' => $row['Discontinued']
            ]);
        }
        return $this;
    }
}
