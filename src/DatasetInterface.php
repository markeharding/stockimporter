<?php
namespace StockImporter;

interface DatasetInterface
{

    /**
     * Return the dataset in an associated array
     * @return array
     */
    public function getDataSet();
}
