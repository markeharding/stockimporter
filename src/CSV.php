<?php
namespace StockImporter;

class CSV implements DatasetInterface
{
    /**
     * @var null|string - the filepath of the csv
     */
    private $filepath = null;

    /**
     * @var string - the delimiter to use
     */
    private $delimiter = ',';

    /**
     * @var array | null - the dataset
     */
    private $dataset;

    /**
     * Set the path of the CSV file
     * @param string $path
     * @return $this
     */
    public function setFilePath($path)
    {
        $this->filepath = $path;
        return $this;
    }

    /**
     * Set the delimiter for the CSV file
     * @param string $delimiter
     * @return $this
     */
    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
        return $this;
    }

    /**
     * Return the dataset in an associated array
     * @return array
     */
    public function getDataset()
    {
        $fo = fopen($this->filepath, "r");
        $header = null;
        while (($row = fgetcsv($fo, 1000, $this->delimiter)) !== false) {
            if (!$header) {
                $header = $row;
                continue;
            }
            $this->dataset[] = array_combine($header, $row);
        }
        fclose($fo);
        return $this->dataset;
    }
}
