<?php
namespace StockImporter;

class Config
{

    /**
     * @var array - the config stroage
     */
    protected $attributes = [];

    public function __construct()
    {
        $this->loadDefaults();
    }

    protected function loadDefaults()
    {
        $json = file_get_contents(dirname(dirname(__FILE__)) . '/conf/config.json');
        $this->attributes = array_merge($this->attributes, json_decode($json, true));
    }

    /**
     * Set a config variable
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function __set($key, $value)
    {
        $this->set($key, $value);
    }

    /**
     * Set a config variable
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function set($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    /**
     * Return a config variable
     * @param string $key
     * @return mixed | null
     */
    public function __get($key)
    {
        return $this->get($key);
    }

    /**
     * Return a config variable
     * @param string $key
     * @return mixed | null
     */
    public function get($key)
    {
        if (isset($this->attributes[$key])) {
            if (is_array($this->attributes[$key])) {
                return (object) $this->attributes[$key];
            }
            return $this->attributes[$key];
        }
        return null;
    }

    /**
     * Return if a config variable has been set
     * @param string $key
     * @return boolean
     */
    public function __isset($key)
    {
        return $this->isset($key);
    }

    /**
     * Return if a config variable has been set
     * @param string $key
     * @return boolean
     */
    public function isset($key)
    {
        if (isset($this->attributes[$key])) {
            return true;
        }
        return false;
    }
}
