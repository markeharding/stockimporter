<?php
namespace StockImporter\Exceptions;

use Exception;

class EmptyDatasetException extends Exception
{

    /**
     * @var string $message
     */
    protected $message = "Not data was found in the dataset";
}
