# Stock Importer

## Installation

Please run `php bin/misc/install.php` to set up the schema

## Importing data

Please run `php bin/misc/import.php PATH_TO_FILE` to import the csv

## Running tests

Please run `./bin/phpspec run`
