<?php

namespace spec\StockImporter;

use StockImporter\CSV;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CSVSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(CSV::class);
    }

    public function it_should_set_a_filepath()
    {
        $this->setFilePath('foobar')->shouldReturn($this);
    }

    public function it_should_set_a_delimiter()
    {
        $this->setDelimiter(',')->shouldReturn($this);
    }

    public function it_should_return_a_dataset_in_an_array()
    {
        $this->setFilePath(dirname(dirname(dirname(__FILE__))) . '/data/stock.csv')->setDelimiter(',');
        $dataset = $this->getDataset();
        $dataset->shouldBeArray();
        $dataset->shouldHaveCount(29);
        $dataset[0]->shouldHaveKeyWithValue('Product Code', 'P0001');
        $dataset[0]->shouldHaveKeyWithValue('Product Name', 'TV');
        $dataset[0]->shouldHaveKeyWithValue('Product Description', '32” Tv');
        $dataset[0]->shouldHaveKeyWithValue('Stock', "10");
        $dataset[0]->shouldHaveKeyWithValue('Cost in GBP', "399.99");
    }
}
