<?php

namespace spec\StockImporter;

use StockImporter\Import;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use StockImporter\Config;
use StockImporter\CSV;
use PDO;
use PDOStatement;

class ImportSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(Import::class);
    }

    public function it_should_throw_exception_during_empty_dataset(CSV $csv)
    {
        $this->shouldThrow('StockImporter\\Exceptions\\EmptyDatasetException')->duringSetDataset($csv);
    }

    public function it_should_add_a_dataset(CSV $csv)
    {
        $csv->getDataset()->willReturn([
          [ 'key' => 'value']
        ]);
        $this->setDataset($csv)->shouldReturn($this);
    }

    public function it_should_import_data(PDO $pdo, PDOStatement $statement, Config $config, CSV $csv)
    {
        $this->beConstructedWith($pdo, $config);

        $dataset = [
          [
            'Product Code' => 'SPEC_P0001',
            'Product Name' => 'SPEC Toaster',
            'Product Description' => 'SPEC Toasts two sides of bread',
            'Stock' => '5',
            'Cost in GBP' => '10.99',
            'Discontinued' => ''
          ],
          [
            'Product Code' => 'SPEC_P0002',
            'Product Name' => 'SPEC Microwave',
            'Product Description' => 'SPEC A great microwave',
            'Stock' => '10',
            'Cost in GBP' => '10.99',
            'Discontinued' => ''
          ]
        ];

        $csv->getDataset()->willReturn($dataset);

        foreach ($dataset as $set) {
            $pdo->prepare("INSERT INTO stock (code, name, description, stock, cost, currency, discontinued) VALUES (:code, :name, :description, :stock, :cost, 'GBP', :discontinued)")->willReturn($statement);
            $statement->execute([
              ':code' => $set['Product Code'],
              ':name' => $set['Product Name'],
              ':description' => $set['Product Description'],
              ':stock' => $set['Stock'],
              ':cost' =>  $set['Cost in GBP'],
              ':discontinued' => $set['Discontinued']
            ])->shouldBeCalled();
        }

        $this->setDataset($csv)->shouldReturn($this);
        $this->import()->shouldReturn($this);
    }
}
