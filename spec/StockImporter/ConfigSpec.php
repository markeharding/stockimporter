<?php

namespace spec\StockImporter;

use StockImporter\Config;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ConfigSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(Config::class);
    }

    public function it_should_set_a_config()
    {
        $this->youSayHello = 'I say goodbye';
        $this->get('youSayHello')->shouldReturn('I say goodbye');
    }

    public function it_should_get_a_config()
    {
        $this->set('welcomeToThe', 'USSR');
        $this->get('welcomeToThe')->shouldReturn('USSR');
    }

    public function it_should_return_null_if_not_set()
    {
        $this->get('rollUpToThe')->shouldReturn(null);
    }

    public function it_should_return_if_a_variable_is_set()
    {
        $this->set('iamThe', 'Walrus');
        $this->isset('iamThe')->shouldReturn(true);
    }

    public function it_should_return_if_a_variable_is_not_set()
    {
        $this->isset('theWalrusIs')->shouldReturn(false);
    }
}
