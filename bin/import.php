<?php
/**
 * Import CLI Interface
 * @author Mark Harding
 */

require_once(dirname(dirname(__FILE__)) . '/vendor/autoload.php');

if (php_sapi_name() !== 'cli') {
    echo "\nError: this script is for the command line only";
    exit;
}

if (!isset($argv[1]) || !$argv[1]) {
    echo "\nPlease provide the path to the CSV file. eg. `php bin/import.php data/stock.csv \n";
    exit;
}

/**
 * First of all, we want to set the CSV file
 */
$csv = new StockImporter\CSV();
$csv->setFilePath($argv[1]);

/**
 * Next, we add our CSV file to the importer, and import to the database
 */
try {
    $importer = new StockImporter\Import();
    $importer->setDataset($csv);
    $importer->import();
} catch (\Exception $e) {
    $error = $e->getMessage();
    echo "\nERROR: $e \n";
    exit;
}

echo "\nSuccessfully imported \n";
