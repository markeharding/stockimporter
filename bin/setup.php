<?php
/**
 * MySQL Schema setup
 * RUN ONCE
 */

require_once(dirname(dirname(__FILE__)) . '/vendor/autoload.php');

if (php_sapi_name() !== 'cli') {
    echo "\nError: this script is for the command line only";
    exit;
}

$config = new StockImporter\Config();

$opts = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false
];
$pdo = new PDO("mysql:host={$config->mysql->host};dbname={$config->mysql->db};charset=utf8", $config->mysql->user, $config->mysql->password, $opts);

//CREATE THE DATABSE
$statement = $pdo->prepare("CREATE DATABASE IF NOT EXISTS {$config->mysql->db}");
$statement->execute();

//CREATE THE TABLE
$statement = $pdo->prepare("CREATE TABLE IF NOT EXISTS stock (code CHAR(5), name VARCHAR(256), description BLOB, stock INT, cost INT, currency VARCHAR(256), discontinued VARCHAR(256), PRIMARY KEY (code))");
$statement->execute();

echo "\nComplete \n";
